﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eticaret.Model.Entity
{
    public class Category : EBase
    {
        public int ParentID { get; set; } = 0; //temel kategori olsun diye
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
