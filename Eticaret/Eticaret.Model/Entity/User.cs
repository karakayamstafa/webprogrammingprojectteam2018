﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eticaret.Model.Entity
{
    public class User : EBase
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string TC { get; set; }
        public string Telephone { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsActive { get; set; }
        public virtual IEnumerable<UserAddress> UserAddresses { get; set; } //tabloda olmaması icin virtual
    }
}
