﻿using Eticaret.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eticaret.UI.WEB.Areas.Admin.Controllers
{
    public class AdminLoginController : Controller
    {
        // GET: Admin/AdminLogin
        TicaretDB db = new TicaretDB();
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string Email, string Password)
        {
            var data = db.Users.Where(x => x.Email == Email && x.Password == Password && x.IsActive == true && x.IsAdmin==true).ToList();
            if (data.Count==1)
            {
                //giris dogru
                Session["AdminLoginUser"] = data.FirstOrDefault();
                return Redirect("/admin"); //yonetici anasayfaya yonlen
            }
            else
            {
                //giris yanlis
                return View();
            }
            
        }
    }
}