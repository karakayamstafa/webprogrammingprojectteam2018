﻿using Eticaret.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eticaret.UI.WEB.Controllers
{
    public class BasketController : TicaretControllerBase
    {
        TicaretDB db = new TicaretDB();
        
        // GET: Basket
        [HttpPost]
        public JsonResult AddProduct(int productID, int quantity)
        {            
            db.Baskets.Add(new Model.Entity.Basket
            {
                CreateDate = DateTime.Now,                
                ProductID = productID,
                Quantity = quantity,
                UserID = LoginUserID
            });
            var ret = db.SaveChanges();
            return Json(ret, JsonRequestBehavior.AllowGet);
        }

        [Route("Sepetim")]
        public ActionResult Index()
        {            
            var data = db.Baskets.Include("Product").Where(x => x.UserID == LoginUserID).ToList();
            return View(data);
        }

        public ActionResult Delete(int id)
        {            
            var deleteitem = db.Baskets.Where(x => x.ID == id).FirstOrDefault();
            db.Baskets.Remove(deleteitem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}