﻿using Eticaret.Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Eticaret.UI.WEB
{
    public class TicaretControllerBase : Controller
    {        
        /// Kullanici Login Kontrolu              
        public bool IsLogin { get; private set; }
        
        /// Giris Yapmis Kisinin IDsi        
        public int LoginUserID { get; set; }
        
        /// Login User Entity        
        public User LoginUserEntity { get; set; }
        protected override void Initialize(RequestContext requestContext)
        {
            //Session["LoginUserID"]
            //Session["LoginUser"]

            if (requestContext.HttpContext.Session["LoginUserID"] != null)
            {
                //kullanici giris yapmis
                IsLogin = true;
                LoginUserID = (int)requestContext.HttpContext.Session["LoginUserID"];
                LoginUserEntity = (Model.Entity.User)requestContext.HttpContext.Session["LoginUser"];
            }

            base.Initialize(requestContext);
        }
    }
}