﻿using Eticaret.Model;
using Eticaret.Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eticaret.UI.WEB.Controllers
{
    public class HomeController : TicaretControllerBase
    {
        TicaretDB db = new TicaretDB();
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.IsLogin = this.IsLogin;
            var data = db.Products.OrderByDescending(x => x.CreateDate).Take(10).ToList();
            return View(data);
        }

        public PartialViewResult GetMenu()
        {
            var db = new TicaretDB();
            var menus = db.Categories.Where(x => x.ParentID == 0).ToList();
            return PartialView(menus);
        }

        [Route("User-Login")]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [Route("User-Login")]
        public ActionResult Login(string Email, string Password)
        {
            var users = db.Users.Where(x => x.Email == Email && x.Password == Password && x.IsActive == true && x.IsAdmin == false).ToList();
            if (users.Count == 1)
            {
                //giris yapildi    
                Session["LoginUserID"] = users.FirstOrDefault().ID;
                Session["LoginUser"] = users.FirstOrDefault();
                return Redirect("/"); //anasayfa demek
            }
            else
            {
                ViewBag.Error = "Hatalı kullanıcı adı veya şifre!";
                return View();
            }
        }

        [Route("User-Create")]
        public ActionResult CreateUser()
        {
            return View();
        }

        [HttpPost]
        [Route("User-Create")]
        public ActionResult CreateUser(User entity)
        {
            try
            {
                entity.CreateDate = DateTime.Now;                
                entity.IsActive = true;
                entity.IsAdmin = false;

                db.Users.Add(entity);
                db.SaveChanges();
                return Redirect("/");
            }
            catch (Exception ex)
            {
                //hata varsa uye kayit sayfasina yonlendirme 
                return View();
            }
        }
    }
}